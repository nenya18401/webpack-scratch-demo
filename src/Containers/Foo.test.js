import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, mount, render } from 'enzyme';

import { MemoryRouter } from 'react-router-dom';
import Foo from './Foo';

describe('A suite', function() {
  it('should render without throwing an error', function() {
    expect(shallow(<Foo />).contains([
      <h1>This is Foo Page</h1>,
    ])).toBe(true);
  });
  it('should be selectable by class "foo"', function() {
    expect(shallow(<Foo />).is('.foo')).toBe(true);
  });
  it('should mount in a full DOM', function() {
    expect(mount(<MemoryRouter><Foo /></MemoryRouter>).find('.foo').length).toBe(1);
  });
  it('should render to static HTML', function() {
    expect(render(<MemoryRouter><Foo /></MemoryRouter>).text()).toEqual('This is Foo PageHome');
  });
});

