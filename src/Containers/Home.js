import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'antd';

const Home = () => (
  <div className="home">
    <h1>Welcome</h1>
    <Button type="danger">
      <NavLink to='/foo'>Foo</NavLink>
    </Button>
  </div>
);

export default Home;
