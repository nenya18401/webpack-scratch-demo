import React, { PropTypes } from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'antd';

import css from './style.css';

const propTypes = {};

const defaultProps = {};

class Foo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={css.foo}>
        <h1>This is Foo Page</h1>
        <Button type="primary">
          <NavLink to='/'>Home</NavLink>
        </Button>
      </div>
    );
  }
}

Foo.propTypes = propTypes;
Foo.defaultProps = defaultProps;

export default Foo;
