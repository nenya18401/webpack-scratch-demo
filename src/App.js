import React from 'react';
import { BrowserRouter, NavLink, Switch, Route } from 'react-router-dom';
import Home from './Containers/Home';
import Foo from './Containers/Foo';
import './App.css';

const App = () => (
  <div className="App">
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Home}></Route>
        <Route exact path='/foo' component={Foo}></Route>
      </Switch>
    </BrowserRouter>
  </div>
);

export default App;
