import { put, takeLatest, all } from 'redux-saga/effects';

function* fetch() {
  const json = yield fetch('https://example.com');
  yield put({ type: 'RECEIVED', json: json.reponse, });
}

function* actionWatcher() {
  yield takeLatest('GET', fetch)
}
export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}