import React from 'react';
import renderer from 'react-test-renderer';

import App from './App';

describe('A suite', function() {
  it('should be good', function() {
    expect(2 + 3).toBe(5);
  });
  it('should match snapshot', function() {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
